import { Selector } from 'testcafe';

fixture `Init`
  .page `http://localhost:8080`;

test('Init project', async (t) => {
  const canvas = await Selector(() => document.querySelector('canvas'));

  await t
    .expect(Selector(() => document.querySelector('canvas')).exists).eql(true)
});
