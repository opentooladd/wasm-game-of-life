extern crate wasm_bindgen;
extern crate bit_vec;

mod utils;

use wasm_bindgen::prelude::*;
use bit_vec::BitVec;

// #[wasm_bindgen] is a macro
// it tells rust to add code to this part after build
// In this case we are using wasm_bindgen to glue our Rust code
// and give it access from Javascript
// #[repr(u8)] tells WebAssembly how we want to
// represent this data in the linear memory of WebAssembly
#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
/// Represent the Cell state
/// Alive or Dead
/// it can be represented as a u8 (a single byte then)
/// because its value are 0 and 1, thus very simple
pub enum Cell {
    Dead = 0,
    Alive = 1,
}

impl From<bool> for Cell {
    fn from(item: bool) -> Self {
        if item == true {
            Cell::Alive
        } else {
            Cell::Dead
        }
    }
}

impl From<Cell> for bool {
    fn from(item: Cell) -> Self {
        if item == Cell::Alive {
            true
        } else {
            false
        }
    }
}

impl From<u32> for Cell {
    fn from(item: u32) -> Self {
        if item == 0 {
            Cell::Dead
        } else {
            Cell::Alive
        }
    }
}

impl From<&u32> for Cell {
    fn from(item: &u32) -> Self {
        if *item == 0 {
            Cell::Dead
        } else {
            Cell::Alive
        }
    }
}

#[derive(Debug, Clone)]
struct Cells(BitVec);

impl Cells {
    fn new() -> Cells {
        Cells(BitVec::new())
    }

    fn get(&self, i: usize) -> bool {
        self.0.get(i).unwrap()
    }

    fn push(&mut self, elem: Cell) {
        self.0.push(elem.into());
    }

    fn set(&mut self, idx: usize, v: bool) {
        self.0.set(idx, v);
    }

    fn as_slice(&self) -> &[u32] {
        self.0.storage()
    }
    fn as_ptr(&self) -> *const u32 {
        self.as_slice().as_ptr()
    }
}

/// Necessary for doing Vec<u8>.iter().collect()
impl<'a> std::iter::FromIterator<&'a u8> for Cells {
    fn from_iter<I: IntoIterator<Item=&'a u8>>(iter: I) -> Self {
        let mut coll = Cells::new();
        for i in iter {
            coll.push(match i {
                0 => Cell::Dead,
                1 => Cell::Alive,
                _ => Cell::Dead,
            });
        }
        coll
    }
}

/// We will represent the Universe has a linear Array of bytes
/// So for a 4x4 Universe we get in linear representation
/// | row 1 | row 2 | row 3 | row 4 |
/// [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
///
/// which in 2D binary would be
/// [
///     [0,0,0,0],
///     [0,0,0,0],
///     [0,0,0,0],
///     [0,0,0,0],
/// ]
#[wasm_bindgen]
#[derive(Debug)]
pub struct Universe {
    width: u32,
    height: u32,
    cells: Cells,
}

/// Impl block describes the methods and constructor associated
/// to a data structure.
/// Here Universe. It has the same name as the Data Structure it handles
#[wasm_bindgen]
impl Universe {
    /// Creates a new Universe
    pub fn new(width: u32, height: u32, cells_in_u8: Vec<u8>) -> Universe {
        Universe { width, height, cells: cells_in_u8.iter().collect() }
    }
    /// Gets the index of a cell by row and column
    pub fn get_index(&self, row: u32, column: u32) -> usize {
        ((row * self.width) + column) as usize
    }
    /// Gets the number of live neigbor for a Cell
    /// to determine that we need to check all 8 sides of a cell
    /// Cell representation: @ = cell, 1 = live neighbor, 0 = dead neighbor
    /// 1 1 1 1 1 0
    /// 1 0 0 0 1 0
    /// 1 0 @ 0 1 0
    /// 1 1 0 0 1 0
    /// 1 1 1 1 1 0
    /// But project on a linear value
    /// |   row 1   |   row 2   |   row 3   |
    /// [1,0,0,0,1,0,1,0,@,0,1,0,1,1,0,0,1,0]
    /// there are cell not necessary to check, x = not necessary to check
    /// [x,0,0,0,x,x,x,0,@,0,x,x,x,1,0,0,x,x]
    pub fn live_neighbor_count(&self, row: u32, column: u32) -> u8 {
        let mut count: u8 = 0;

        // based on the cell index, we will perform a translation
        // on the precedent and next row
        let prev_translation_row_cell = if row >= 1 {
            Some((row - 1, column))
        } else {
            None 
        };
        let next_translation_row_cell = if row < self.height - 1 {
            Some((row + 1, column))
        } else {
            None
        };

        // right cells
        if column >= 1 {
            // get the prev column of the actual cell
            count += self.cells.get(self.get_index(row, column - 1)) as u8;

            match prev_translation_row_cell {
                Some((x, y)) => { 
                    count += self.cells.get( self.get_index(x, y - 1) ) as u8;
                }
                None => {}
            }
            match next_translation_row_cell {
                Some((x, y)) => { 
                    count += self.cells.get( self.get_index(x, y - 1) ) as u8;
                }
                None => {}
            }
        };

        // left cells
        if column < self.width - 1 {
            // get the next column of the actual cell
            count += self.cells.get( self.get_index(row, column + 1) ) as u8;

            match prev_translation_row_cell {
                Some((x, y)) => { 
                    count += self.cells.get( self.get_index(x, y + 1) ) as u8;
                }
                None => {}
            }
            match next_translation_row_cell {
                Some((x, y)) => { 
                    count += self.cells.get( self.get_index(x, y + 1) ) as u8;
                }
                None => {}
            }
        } 
        
        // center cell
        match prev_translation_row_cell {
            Some((x, y)) => {
                count += self.cells.get( self.get_index(x , y) ) as u8;
            }
            None => {}
        };
        match next_translation_row_cell {
            Some((x, y)) => {
                count += self.cells.get( self.get_index(x , y) ) as u8;
            }
            None => {}
        };

        count
    }

    /// Compute the next state of the Universe
    /// by applying the rules of the Game of Life.
    /// 1) Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
    /// 2) Any live cell with two or three live neighbours lives on to the next generation.
    /// 3) Any live cell with more than three live neighbours dies, as if by overpopulation.
    /// 4) Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
    pub fn tick(&mut self) {
        for row in 0..self.height {
            for col in 0..self.width {
                let idx = self.get_index(row, col);
                let cell = self.cells.get( idx );
                let live_neighbors = self.live_neighbor_count(row, col);

                let next_cell = match (cell.into(), live_neighbors) {
                    // Rule 1: Any live cell with fewer than two live neighbours
                    // dies, as if caused by underpopulation.
                    (Cell::Alive, x) if x < 2 => Cell::Dead,
                    // Rule 2: Any live cell with two or three live neighbours
                    // lives on to the next generation.
                    (Cell::Alive, 2) | (Cell::Alive, 3) => Cell::Alive,
                    // Rule 3: Any live cell with more than three live
                    // neighbours dies, as if by overpopulation.
                    (Cell::Alive, x) if x > 3 => Cell::Dead,
                    // Rule 4: Any dead cell with exactly three live neighbours
                    // becomes a live cell, as if by reproduction.
                    (Cell::Dead, 3) => Cell::Alive,
                    // All other cells remain in the same state.
                    (otherwise, _) => otherwise,
                };

                self.cells.set(idx, next_cell.into());
            }
        }
    }

    /// Getter to the cells vector for Javascript
    pub fn cells(&self) -> *const u32 {
        self.cells.as_ptr()
    }
}
