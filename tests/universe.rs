extern crate wasm_game_of_life;
use wasm_game_of_life::{ Universe };

pub fn cells_from_2d_vec(ar: Vec<Vec<u8>>) -> Vec<u8> {
    ar.iter().fold(vec![], |mut acc, row| {
        row.iter().for_each(|col| {
            acc.push(col.clone());
        });
        acc
    })
}

#[test]
fn test_live_neighbor_1_live() {
    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 0, 0, 1, 1],
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(2, 2), 1);

    let cells = cells_from_2d_vec(vec![
        vec![1, 0, 1, 0, 1, 1],
        vec![0, 1, 1, 0, 1, 1],
        vec![1, 1, 1, 0, 1, 1],
        vec![0, 0, 0, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 0), 1);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 0, 0, 1, 1],
        vec![1, 0, 0, 0, 1, 1],
        vec![1, 1, 1, 0, 1, 1],
        vec![0, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(5, 0), 1);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 0],
        vec![1, 1, 1, 1, 0, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(5, 5), 1);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 0, 1],
        vec![1, 1, 1, 1, 1, 0],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 5), 1);

    let cells = cells_from_2d_vec(vec![
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 0, 0, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 2), 1);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![0, 0, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![0, 0, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(3, 0), 1);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 0, 0, 1, 1],
        vec![1, 0, 1, 0, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(5, 2), 1);
}

#[test]
fn test_live_neighbor_2_live() {
    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 1, 0, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(2, 2), 2);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 0), 2);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(5, 0), 2);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 0, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(5, 5), 2);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 0, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 5), 2);

    let cells = cells_from_2d_vec(vec![
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 1, 0, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 2), 2);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
        vec![0, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(3, 0), 2);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 0, 1],
        vec![1, 1, 1, 1, 0, 1],
        vec![1, 1, 1, 1, 0, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(4, 5), 2);
}

#[test]
fn test_live_neighbor_3_live() {
    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 0, 0, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(2, 2), 3);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 0, 1, 1],
        vec![1, 1, 1, 0, 1, 1],
        vec![1, 1, 1, 0, 1, 1],
        vec![0, 0, 0, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 0), 3);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 0, 0, 1, 1],
        vec![1, 0, 0, 0, 1, 1],
        vec![1, 1, 1, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(5, 0), 3);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(5, 5), 3);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 5), 3);

    let cells = cells_from_2d_vec(vec![
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 2), 3);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![0, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(3, 0), 3);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 0, 1, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(5, 2), 3);
}

#[test]
fn test_live_neighbor_4_live() {
    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 0, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(2, 2), 4);

    let cells = cells_from_2d_vec(vec![
        vec![1, 0, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 2), 4);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(3, 0), 4);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(5, 2), 4);
}

#[test]
fn test_live_neighbor_5_live() {
    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(2, 2), 5);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(0, 2), 5);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(3, 0), 5);

    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(5, 2), 5);
}

#[test]
fn test_live_neighbor_6_live() {
    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 0, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(2, 2), 6);
}

#[test]
fn test_live_neighbor_7_live() {
    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 0, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(2, 2), 7);
}

#[test]
fn test_live_neighbor_8_live() {
    let cells = cells_from_2d_vec(vec![
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 1, 1],
    ]);
    let universe = Universe::new(6, 6, cells);
    assert_eq!(universe.live_neighbor_count(2, 2), 8);
}
