//! Test suite for the Web and headless browsers.

#![cfg(target_arch = "wasm32")]

extern crate wasm_bindgen_test;
use wasm_bindgen_test::*;
use wasm_game_of_life::{ Universe };

wasm_bindgen_test_configure!(run_in_browser);

pub fn cells_from_2d_vec(ar: Vec<Vec<u8>>) -> Vec<u8> {
    ar.iter().fold(vec![], |mut acc, row| {
        row.iter().for_each(|col| {
            acc.push(col.clone());
        });
        acc
    })
}

#[wasm_bindgen_test]
fn test_render() {
    let cells = cells_from_2d_vec(vec![
        vec![0, 0, 0, 0, 0, 0],
        vec![0, 0, 0, 1, 1, 0],
        vec![0, 0, 1, 0, 0, 0],
        vec![0, 0, 0, 1, 0, 0],
        vec![0, 0, 0, 0, 0, 0],
        vec![0, 0, 0, 0, 0, 0],
    ]);
    let universe = Universe::new(6, 6, cells);
}
