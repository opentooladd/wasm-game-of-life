import { memory } from 'wasm-game-of-life/wasm_game_of_life_bg';
import { Universe } from 'wasm-game-of-life';

// CANVAS CONSTANTS
const BORDER_WIDTH = 1; // px
const CELL_SIZE = 5; // px
const CELL_SIZE_PLUS_BORDER = CELL_SIZE + BORDER_WIDTH;
const GRID_COLOR = "#CCCCCC";
const DEAD_COLOR = "#FFFFFF";
const ALIVE_COLOR = "#000000";

// UNIVERSE CONSTANTS
const WIDTH = (window.innerWidth / 2) / CELL_SIZE;
const HEIGHT = (window.innerHeight / 2) / CELL_SIZE;


const CANVAS_WIDTH = WIDTH * CELL_SIZE_PLUS_BORDER;
const CANVAS_HEIGHT = HEIGHT * CELL_SIZE_PLUS_BORDER;
const CELL_NUMBER = Math.floor(WIDTH * HEIGHT);

// construct an array of cells with an interesting pattern
// the cell array must always be an integer, hence the Mawth.floor call
const cells = [...Array(CELL_NUMBER).keys()].map((_, i) => {
  if (Math.random() > 0.9) {
    return 1;
  } else {
    return 0;
  }
});

const universe = Universe.new(WIDTH, HEIGHT, cells);

const canvas = document.getElementById("game-of-life-canvas");
const canvas_ctx = canvas.getContext('2d');

// give the canvas the size needed for all the cells
// + a 1 px border around each of them
canvas.width = CANVAS_WIDTH + BORDER_WIDTH;
canvas.height = CANVAS_HEIGHT + BORDER_WIDTH;

const drawGrid = () => {
  canvas_ctx.beginPath();
  canvas_ctx.strokeStyle = GRID_COLOR;

  // draw the horizontal grid
  for (let i = 0; i <= WIDTH; i++) {
    canvas_ctx.moveTo(i * CELL_SIZE_PLUS_BORDER, 0);
    canvas_ctx.lineTo(i * CELL_SIZE_PLUS_BORDER, CANVAS_HEIGHT + BORDER_WIDTH);
  }

// draw the vertical grid
  for (let i = 0; i <= HEIGHT; i++) {
    canvas_ctx.moveTo(0, i * CELL_SIZE_PLUS_BORDER);
    canvas_ctx.lineTo(CANVAS_WIDTH + BORDER_WIDTH, i * CELL_SIZE_PLUS_BORDER);
  }

  canvas_ctx.stroke();
};

const bitIsSet = (n, arr) => {
  const byte = Math.floor(n / 8);
  const mask = 1 << (n % 8);
  return (arr[byte] & mask) === mask;
};

/* We can directly access WebAssembly's linear memory via memory, which is defined in the raw wasm module wasm_game_of_life_bg.
 * To draw the cells, we get a pointer to the universe's cells, construct a Uint8Array overlaying the cells buffer, iterate over each cell, and draw a white or black rectangle depending on whether the cell is dead or alive, respectively.
 * By working with pointers and overlays, we avoid copying the cells across the boundary on every tick.
*/
const drawCells = () => {
  const cellsPtr = universe.cells();
  // see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint8Array
  /*
   * In JavaScript, constructing a Uint8Array from Wasm memory is the same as before, except that the length of the array is not width * height anymore, but width * height / 8 since we have a cell per bit rather than per byte
   */
  const cellsArray = new Uint8Array(memory.buffer, cellsPtr, (WIDTH * HEIGHT) / 8);

  canvas_ctx.beginPath();

  // cycle on the cells
  for (let row = 0; row < HEIGHT; row++) {
    for (let col = 0; col < WIDTH; col++) {
      const index = universe.get_index(row, col);

      const fillColor = bitIsSet(index, cellsArray) ? ALIVE_COLOR : DEAD_COLOR;
      

      canvas_ctx.fillStyle = fillColor;
      canvas_ctx.fillRect(
        col * CELL_SIZE_PLUS_BORDER,
        row * CELL_SIZE_PLUS_BORDER,
        CELL_SIZE,
        CELL_SIZE
      );
    }
  }

  canvas_ctx.stroke();
};

const renderLoop = () => {
  universe.tick();

  drawGrid();
  drawCells();

  requestAnimationFrame(renderLoop);
};
requestAnimationFrame(renderLoop);
